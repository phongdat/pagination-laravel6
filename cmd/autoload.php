<?php
require __DIR__.'/../vendor/autoload.php';

use Illuminate\Contracts\Console\Kernel;

class CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function init()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}

$createsApplication = new CreatesApplication();
$createsApplication->init();
?>
